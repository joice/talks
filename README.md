## talks
1. Seminar
    * [Presentation](https://gitlab.com/gecw/presentation/-/jobs/62964322/artifacts/file/presentation.pdf)
    * [Report](https://gitlab.com/gecw/seminar-report/-/jobs/62965479/artifacts/file/thesis.pdf)
    * [Synopsis](https://gitlab.com/gecw/seminar-synopsis/-/jobs/62966120/artifacts/file/synopsis.pdf)
2. [Introduction to Platform cooperativism - അ മാസിക](https://cloud.disroot.org/s/m8mJjrb5asLWz2Q)
3. [Free Software - Don Bosco College, S. Bathery](https://cloud.disroot.org/s/Y9BgAGBorFba6Q9)